## how to start with this project:

### Clone

git clone https://github.com/ihebrejeb/kimy-front.git<br>
cd kimy-front

### install yarn if you don't have it

npm install --global yarn

### Install dependencies

yarn install

### Start project

yarn start
